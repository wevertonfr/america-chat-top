package br.com.americasaude.testeamerica.model

data class Message(
    var id: String,
    var texto: String,
    var usuario: String,
    var horario: Long
)