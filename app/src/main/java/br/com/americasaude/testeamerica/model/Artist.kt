package br.com.americasaude.testeamerica.model

data class Artist(
    var id: String,
    var name: String,
    var genre: String
)
