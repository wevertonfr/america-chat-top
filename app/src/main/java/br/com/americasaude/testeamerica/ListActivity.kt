package br.com.americasaude.testeamerica

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import com.google.firebase.database.*

class ListActivity : AppCompatActivity() {

    private lateinit var listaArtistas: ListView
    val dbRef: DatabaseReference = FirebaseDatabase.getInstance().reference.child("artist")
    var artistsArr = arrayListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        listaArtistas = findViewById(R.id.lstLAartist)
        dbRef.addValueEventListener(object: ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented")
            }
            override fun onDataChange(snapshot: DataSnapshot) {
                addDataToList(snapshot)
                val adapter = ArrayAdapter<String>(this@ListActivity,
                    android.R.layout.simple_list_item_1,
                    android.R.id.text1,
                    artistsArr)
                listaArtistas.adapter = adapter
            }
        })
    }

    private fun addDataToList(snapshot: DataSnapshot) {
        val interator = snapshot.children.iterator()
        artistsArr.clear()

        while(interator.hasNext()) {
            val item = interator.next()
            val json = item.value as HashMap<Any, Any>

            artistsArr.add(json.getValue("name") as String)
        }
    }
}
