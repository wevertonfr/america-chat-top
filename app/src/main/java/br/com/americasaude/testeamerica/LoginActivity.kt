package br.com.americasaude.testeamerica

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.content.Intent
import android.widget.EditText


class LoginActivity : AppCompatActivity() {

    lateinit var btnSaveNick: Button
    lateinit var edtNickName: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        edtNickName = findViewById(R.id.editLAnick)
        btnSaveNick = findViewById(R.id.btnLogin)

        btnSaveNick.setOnClickListener {
            val name = edtNickName.text.toString()
            val intent = Intent(this@LoginActivity, ChatActivity::class.java)
            intent.putExtra("Name", name)
            startActivity(intent)
        }
    }
}

private fun Intent.putExtra(s: String, edtNickName: TextView?) {

}
