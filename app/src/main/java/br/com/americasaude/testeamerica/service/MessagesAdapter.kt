package br.com.americasaude.testeamerica.service

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.view.LayoutInflater
import android.widget.TextView
import br.com.americasaude.testeamerica.R
import br.com.americasaude.testeamerica.model.Message
import java.util.*
import java.util.concurrent.TimeUnit


class MessagesAdapter(private val context: Context, private val list: ArrayList<Message>, private val currentUser: String) : BaseAdapter() {

    private var inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = inflater.inflate(R.layout.layout_message, null, false)
        //Adapter gives position parameter.
        //This parameter helps us to know which item view is wanted by adapter.
        (view.findViewById<TextView>(R.id.txtNome)).text = list[position].usuario
        (view.findViewById<TextView>(R.id.txtMessage)).text = list[position].texto
        (view.findViewById<TextView>(R.id.txtHora)).text = convertMillisToString(list[position].horario)

        val constraintSet = ConstraintSet()
        val container = view.findViewById<ConstraintLayout>(R.id.container)
        val dadContainer = view.findViewById<ConstraintLayout>(R.id.dadContainer)

        if (list[position].usuario == currentUser.trim()) {
            container.run {
                background = context.getDrawable(R.drawable.st_txt_sender)
                constraintSet.clone(dadContainer)
                constraintSet.clear(this.id, ConstraintSet.START)
            }
        } else {
            constraintSet.clone(dadContainer)
            constraintSet.clear(container.id, ConstraintSet.END)
        }
        constraintSet.applyTo(dadContainer)

        return view
    }
    private fun convertMillisToString(time: Long): String {
        val data = Date(time)
        val hh = data.hours
        val mm = data.minutes

        return String.format("%02d:%02d", hh, mm)
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return list.size
    }
}