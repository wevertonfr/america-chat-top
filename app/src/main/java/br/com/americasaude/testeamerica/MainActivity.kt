package br.com.americasaude.testeamerica

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import android.widget.Spinner
import br.com.americasaude.testeamerica.model.Artist
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var editName: EditText
    lateinit var btnAddArtist: Button
    lateinit var btnLista: Button
    lateinit var btnChat: Button
    lateinit var selectGenre: Spinner

    val dbRef: DatabaseReference = FirebaseDatabase.getInstance().getReference("artist")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        startActivity(Intent(this@MainActivity, LoginActivity::class.java))

        editName = findViewById(R.id.editMAname)
        btnAddArtist = findViewById(R.id.btnAddArtist)
        btnLista = findViewById(R.id.btnMAlista)
        btnChat = findViewById(R.id.btnMAchat)
        selectGenre = findViewById(R.id.spinnerGenres)

        btnAddArtist.setOnClickListener { addArtist()}
        btnLista.setOnClickListener {
            startActivity(Intent(this@MainActivity, ListActivity::class.java))
        }
        btnChat.setOnClickListener {
            startActivity(Intent(this@MainActivity, LoginActivity::class.java))
        }

    }

    private fun addArtist(){
        val name: String = editName.text.toString().trim()
        val genre: String = spinnerGenres.selectedItem.toString()

        if (name.isEmpty()){
            Toast.makeText(this@MainActivity, "Selecione um nome válido", Toast.LENGTH_LONG).show()
        }else{
            val id: String = dbRef.push().key!!
            dbRef.child(id).setValue(Artist(id, name, genre))
            Toast.makeText(this@MainActivity, "Artista adidionado", Toast.LENGTH_LONG).show()
        }
    }
}
