package br.com.americasaude.testeamerica

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*
import br.com.americasaude.testeamerica.model.Message
import br.com.americasaude.testeamerica.service.MessagesAdapter
import com.google.firebase.database.*
import com.google.firebase.messaging.FirebaseMessaging
import kotlin.math.log

class ChatActivity : AppCompatActivity() {

    private lateinit var exibeNick: TextView
    private lateinit var mensagensView: ListView
    private lateinit var edtMessage: EditText
    private lateinit var btnSend: ImageButton

    val dbRef: DatabaseReference = FirebaseDatabase.getInstance().reference.child("mensagens")
    var messagesArr = arrayListOf<Message>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        FirebaseMessaging.getInstance().subscribeToTopic("chattop").addOnCompleteListener { task ->
            var msg = "Bem vindo a sessão"
            if (!task.isSuccessful) {
                msg = "tchauuuuuu"
            }
            Log.d("firebase", msg)
            Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
        }

        val intent = intent
        val name = intent.getStringExtra("Name")

        exibeNick = findViewById(R.id.nickNameUser)
        exibeNick.text = name

        mensagensView = findViewById(R.id.lstCAmensagens)
        edtMessage = findViewById(R.id.edtCAmessage)
        btnSend = findViewById(R.id.btnCAsend)

        btnSend.setOnClickListener { sendMessage() }

        dbRef.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(err: DatabaseError) {
                Log.e("start", err.message)
            }
            override fun onDataChange(snapshot: DataSnapshot) {
                addDataToList(snapshot)
                val adapter = MessagesAdapter(
                    this@ChatActivity,
                    messagesArr,
                    name)
                mensagensView.divider = null
                mensagensView.dividerHeight = 10
                mensagensView.adapter = adapter
            }
        })
    }

    private fun sendMessage(){
        val txtMessage: String = edtMessage.text .toString().trim()
        val horario: Long = System.currentTimeMillis()
        val usuario: String = exibeNick.text.toString().trim()

        if (txtMessage.isEmpty()){
            Toast.makeText(this@ChatActivity, "Digite uma mensagem", Toast.LENGTH_LONG).show()
        }else{
            val id: String = dbRef.push().key!!
            dbRef.child(id).setValue(Message(id, txtMessage, usuario, horario))
            Toast.makeText(this@ChatActivity, "Mensagem enviada", Toast.LENGTH_LONG).show()
        }
        edtMessage.text.clear()
    }

    private fun addDataToList(snapshot: DataSnapshot) {
        val interator = snapshot.children.iterator()
        messagesArr.clear()

        while(interator.hasNext()) {
            val item = interator.next()
            val json = item.value as HashMap<Any, Any>

            messagesArr.add(Message(
                json.getValue("id") as String,
                json.getValue("texto") as String,
                json.getValue("usuario") as String,
                json.getValue("horario") as Long
            ))
        }
    }
}